import { Component } from '@angular/core';
import { FormControl, FormBuilder, FormGroup,FormArray } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent {
  taskForm: FormGroup;  
  taskArray = []
  tasks: any;

  constructor(private fb:FormBuilder) {  }

  ngOnInit(){
    
    this.taskForm = this.fb.group({  
      selectDate: moment(new Date()).format('YYYY-MM-DD'),
      startTime: ['00:00'],
      endTime: ['12:00'],
      description: [''] 
    }); 
    

  }

  addTask() { 
   this.taskArray.push(this.taskForm.value)
    console.log(this.taskArray)
  }

  removeTask(i:number) {  
    delete this.taskArray[i];
  }  
}
